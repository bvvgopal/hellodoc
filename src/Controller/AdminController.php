<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class AdminController extends EasyAdminController
{   
	public function listPatientAction()
    {		
        /***** OVERRIDE PART *****/
		$user = $this->getUser();
		if ($this->isGranted("ROLE_DOCTOR") && !$this->isGranted("ROLE_ADMIN")) {
		    $this->entity['list']['dql_filter'] = "entity.user = ".$user->getId();
        }else
		{
			$this->entity['list']['dql_filter'] ='';
		}
		       
        /***** END OVERRIDE *****/
        return parent::listAction();
    }
	public function searchPatientAction()
    {		
        /***** OVERRIDE PART *****/
		$user = $this->getUser();
		if ($this->isGranted("ROLE_DOCTOR") && !$this->isGranted("ROLE_ADMIN")) {
		    $this->entity['list']['dql_filter'] = "entity.user = ".$user->getId();
        }else
		{
			$this->entity['list']['dql_filter'] ='';
		}
		       
        /***** END OVERRIDE *****/
        return parent::listAction();
    }
}
