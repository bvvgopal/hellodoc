<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name',TextType::class, [
                'label' => 'firstname',
				'translation_domain' => 'FOSUserBundle',
                
            ])
			->add('last_name',TextType::class, [
                'label' => 'lastname',
				'translation_domain' => 'FOSUserBundle',
                
            ])
			->add('phone_no',TextType::class, [
                'label' => 'phoneno',
				'translation_domain' => 'FOSUserBundle',
            ])
		;
    }

    public function getParent()
    {
       return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    public function getBlockPrefix()
    {
       return 'app_user_registration';
    }
    public function getName()
    {
       return $this->getBlockPrefix();
    }

}
