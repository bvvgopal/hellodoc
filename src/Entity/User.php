<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{ 
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
	
	/**
    * @ORM\Column(type="string", length=255)
    */
    protected $first_name;
   
    /**
    * @ORM\Column(type="string", length=255,  nullable=false)
    */
    protected $last_name;
	
	/**
    * @ORM\Column(type="string", length=10)
    */
    protected $phone_no;

    public function getId(): ?int
    {
        return $this->id;
    }
	
	/**
    * Set firstName
    *
    * @param string $firstName
    *
    * @return User
    */
   public function setFirstName($firstName)
   {
       $this->first_name = $firstName;
       return $this;
   }
   /**
    * Get firstName
    *
    * @return string
    */
   public function getFirstName()
   {
       return $this->first_name;
   }
   /**
    * Set lastName
    *
    * @param string $lastName
    *
    * @return User
    */
   public function setLastName($lastName)
   {
       $this->last_name = $lastName;
       return $this;
   }
   /**
    * Get lastName
    *
    * @return string
    */
   public function getLastName()
   {
       return $this->last_name;
   }
    
	/**
    * Set phoneNo
    *
    * @param string $phoneNo
    *
    * @return User
    */
   public function setphoneNo($phoneNo)
   {
       $this->phone_no = $phoneNo;
       return $this;
   }
   /**
    * Get phoneNo
    *
    * @return string
    */
   public function getphoneNo()
   {
       return $this->phone_no;
   }


}
