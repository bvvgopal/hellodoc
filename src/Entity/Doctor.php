<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DoctorRepository")
 */
class Doctor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=20)* @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
	
     */
    private $phone;
	
	/**
     * @ORM\Column(type="string", length=20, unique=true)
	 
     */
    private $appointment_number;
	
	/**
     * @ORM\Column(type="string", length=250)
	 
     */
    private $category;
	
	/**
     * @ORM\Column(type="string", length=250)
	 
     */
    private $specialization;
	
	/**
     * @ORM\Column(type="text")
	 
     */
    private $clinic_address;
	
	/**
     * @ORM\OneToOne(targetEntity="App\Entity\User",cascade={"all"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;
	
	
	/**
     * @ORM\Column(type="text")
	 
     */
    private $description;
	
	/**
     * @ORM\Column(type="string")
	 
     */
    private $image;
	
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
	
	public function getAppointmentNumber(): ?string
    {
        return $this->appointment_number;
    }

    public function setAppointmentNumber(string $appointment_number): self
    {
        $this->appointment_number = $appointment_number;

        return $this;
    }
	
	public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }
	
	public function getSpecialization(): ?string
    {
        return $this->specialization;
    }

    public function setSpecialization(string $specialization): self
    {
        $this->specialization = $specialization;

        return $this;
    }
	
	public function getClinicAddress(): ?string
    {
        return $this->clinic_address;
    }

    public function setClinicAddress(string $clinic_address): self
    {
        $this->clinic_address = $clinic_address;

        return $this;
    }
	/**
     * Set user
     *
     * @param \App\Entity\User $user
     * @return Patient
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;
 
        return $this;
    }
 
    /**
     * Get user
     *
     * @param \App\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
	
	public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
	
	public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
